# tiny-reminder

Short-span reminder with desktop notifications (using `notify-send`). Notifications can be repetitive. (e.g. "Off-screen break" every 2 hours, etc.) It can set reminders only for the next 24 hours maximum. (More accurately, 23:59.)

## License

GNU General Public License, version 3.

## Requirements

- **notify-send**. (The Linux one. I don't know if tiny-reminder will work with the `notify-send` that is ported to other operating systems.)

- **Ruby**.

## Usage

**`tiny-reminder.rb TIME [options]`**

`TIME` must be less than 24 hours and it can be given in the following formats:

- An integer number for minutes until the reminder.
(e.g. **`tiny-reminder.rb 90`** for reminder in 1 and a half hour.)

- An integer or decimal number followed by **`h`** for hours until the reminder.
(e.g. **`tiny-reminder.rb 1.25h`** for reminder in 1 hour and 15 minutes.)

- In H:MM format for the real-clock time for the reminder.
(e.g. **`tiny-reminder.rb 21:20`** for reminder at 9:20 PM.) --**_Note_**: repetitions are not accepted with this format.

### Options

**`-r, --repeat REPETITIONS`** &emsp;&emsp;Times to repeat the notification. If a hyphen is given instead of an Integer, notifications repeat endlessly. (They will not stop on the 24-hour limit.)

**`-m, --message MESSAGE`** &emsp;&emsp;The message of the reminder.

**`-h, --help`** &emsp;&emsp;Show help message.

### Examples

**`tiny-reminder.rb 20 -r 6`** (=>Notification every 20 minutes, 6 times.)

**`tiny-reminder.rb -r - 1.75h -m 'Take a break'`** (=>Notification every 1 hour and 45 minutes, infinite number of repetitions, with the message "Take a break".)

**`tiny-reminder.rb 9:30 -m 'Call Mike'`** (=>Notification at 9:30 AM, with the message "Call Mike".)

## Notes

- When a reminder is successfully scheduled, a notification is immediately displayed on the desktop. If this initial notification does not appear, it means that there was some error and the intended reminder will not be executed.

- Tiny-reminder does not use persistent storage so your scheduled reminders will not survive a reboot.

- If you `hibernate` or `sleep` your computer, scheduled reminders will be delayed by the suspension time.

## Translations

If you want to add your translation to tiny-reminder, the basic guideline is below. Taking a look at the "**translations.yml**" file would make things even clearer to you.

1. Type **`echo $LANG | sed 's/\..*//'`** in your terminal and copy its output to the end of 'translations.yml'. Then add a colon (`:`) to the right of it.

2. Below that line, copy the left list of words/phrases from any translation set that you find in 'translations.yml'. Have every word indented with two spaces from the left, put a colon (`:`) and then a space to the right of the word, and then write your translation of the word with appropriate capitalization.\
If the order of your translated words doesn't sound right in your language during usage of tiny-reminder, you can try to reassign your translated words to other words.

3. Feel free to create a pull request or just open an issue that contains your translation and your `$LANG` variable and I will add your translation myself. Whichever you find most convenient. **Thank you!**

#### Currently translated in:

• **el-GR** by [Tokros](https://codeberg.org/Tokros)

• **en-US** by [Tokros](https://codeberg.org/Tokros)
