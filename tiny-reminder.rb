#!/usr/bin/env ruby

### BEGIN - Parse options.
require 'optparse'

options = {}
optparse = OptionParser.new do |opts|
	opts.banner = "Usage: tiny-reminder.rb TIME [options]\n\nTIME must be less than 24 hours and it can be given in the following formats:\n1. As Integer for minutes until the reminder.\n (e.g. 'tiny-reminder.rb 90' for reminder in 1 and a half hour.)\n2. As Integer or Decimal number followed by h for hours until the reminder.\n (e.g. 'tiny-reminder.rb 1.25h' for reminder in 1 hour and 15 minutes.)\n3. In H:MM format for the real-clock time for the reminder.\n (e.g. 'tiny-reminder.rb 21:20' for reminder at 9:20 PM.) -Note: repetitions are not accepted with this format.\n\nOther options are:\n"
	opts.on("-r", "--repeat REPETITIONS", "Times to repeat the notification. If a hyphen is given, notifications repeat endlessly. (They will not stop on the 24-hour limit.)") do |r|
		if r.strip == '-'
			options[:repetitions] = "\u221E"
		elsif r.strip =~ /^\d+$/
				options[:repetitions] = r.to_i if r.to_i >= 2
		else
			abort(optparse.help)
		end
	end
	opts.on("-m", "--message MESSAGE", "The message of the reminder.") do |m|
	options[:message] = m
	end
	opts.on("-h", "--help", "Show help message.") do
  puts opts
  exit
	end
end
optparse.parse!
### END - Parse options.

### BEGIN - Parse desired timespan (SPANAKI(seconds)) from user input.
if ARGV.size == 1
	dont_accept_repetitions = false
	if ARGV[0] =~ /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/
		NOW = Time.now
		parsed_HH_MM = Time.new(NOW.year,NOW.month,NOW.day,ARGV[0].split(':')[0].to_i,ARGV[0].split(':')[1].to_i,0)
		parsed_HH_MM += 86400 if parsed_HH_MM < NOW
		SPANAKI = (parsed_HH_MM - NOW).ceil()
		exit if (SPANAKI) / 60 < 1
		dont_accept_repetitions = true

	elsif ARGV[0] =~ /^[0-9]*.?[0-9]+h$/ && ((1.0 / 1440.0)..(1439.0 / 60.0)).include?(ARGV[0].chop.to_f)
		SPANAKI = (ARGV[0].to_f * 3600).round()

	elsif ARGV[0] =~ /^[0-9]+$/ && (1..1439).include?(ARGV[0].to_i)
		SPANAKI = ARGV[0].to_i * 60
	else
		abort(optparse.help)
	end
else
	abort(optparse.help)
end
### END - Parse desired timespan (SPANAKI(seconds)) from user input.

### BEGIN - Get translation
require 'yaml'
translations_file = YAML.load_file(File.join(__dir__, 'translations.yml'))
user_lang = `echo $LANG`.split('.')[0]
user_lang = 'en_US' if ! translations_file.has_key?(user_lang)
### END - Get translation

### BEGIN - Form span_description (for notification body).
span_description = String.new

if (SPANAKI % 3600) % 60 > 0
	if (SPANAKI % 3600) / 60 == 59
		HOURS = (SPANAKI / 3600) + 1
		MINUTES = 0
	else
		HOURS = SPANAKI / 3600
		MINUTES = ((SPANAKI % 3600) / 60) + 1
	end
else
	HOURS = SPANAKI / 3600
	MINUTES = (SPANAKI % 3600) / 60
end

if HOURS == 1
	span_description += "1 #{translations_file[user_lang]['hour']}"
elsif HOURS > 1
	span_description += "#{HOURS} #{translations_file[user_lang]['hours']}"
end

if HOURS >= 1 && MINUTES >= 1
	span_description += " #{translations_file[user_lang]['and']} "
end

if MINUTES == 1
	span_description += "1 #{translations_file[user_lang]['minute']}"
elsif MINUTES > 1
	span_description += "#{MINUTES} #{translations_file[user_lang]['minutes']}"
end
### END - Form span_description (for notification body).

### BEGIN - Form NOTIFICATION_HEADER.
if ! options.has_key?(:message)
	INITIAL_NOTIFICATION_HEADER = "#{translations_file[user_lang]['Notification']}"
	NOTIFICATION_HEADER = "#{translations_file[user_lang]['Time expired']}"
else
	INITIAL_NOTIFICATION_HEADER = options[:message]
	NOTIFICATION_HEADER = options[:message]
end
### END - Form NOTIFICATION_HEADER.

### BEGIN - Form notification body.
notification_body = span_description
if HOURS + MINUTES >= 2
  notification_body += " #{translations_file[user_lang]['have passed']}."
else
  notification_body += " #{translations_file[user_lang]['has passed']}."
end
### END - Form notification body.

if options.has_key?(:repetitions) && dont_accept_repetitions == false
	system("notify-send \"#{INITIAL_NOTIFICATION_HEADER} #{translations_file[user_lang]['at']} #{(Time.now + SPANAKI).strftime("%R")}\" \"#{translations_file[user_lang]['In']} #{span_description}.\n(#{options[:repetitions]} #{translations_file[user_lang]['repetitions']})\" -i preferences-system-time")
	fork do
		(1..).each do |counter|
			sleep SPANAKI
			system("notify-send \"#{NOTIFICATION_HEADER}\" \"#{notification_body}\n(#{translations_file[user_lang]['Repetition']} #{counter} #{translations_file[user_lang]['of']} #{options[:repetitions]})\" -t 0 -i preferences-system-time")
			break if counter.to_s == options[:repetitions].to_s # Ugly but works ok.
		end
	end

else
	system("notify-send \"#{INITIAL_NOTIFICATION_HEADER} #{translations_file[user_lang]['at']} #{(Time.now + SPANAKI).strftime("%R")}\" \"#{translations_file[user_lang]['In']} #{span_description}.\" -i preferences-system-time")
	fork do
		sleep SPANAKI
		system("notify-send \"#{NOTIFICATION_HEADER}\" \"#{notification_body}\" -t 0 -i preferences-system-time")
	end
end
